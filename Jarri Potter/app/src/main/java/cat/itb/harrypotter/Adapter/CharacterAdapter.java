package cat.itb.harrypotter.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.OnClick;
import cat.itb.harrypotter.R;
import cat.itb.harrypotter.retrofit.HogwartsCharacter;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>{

    OnCharacterClickedListener onCharacterClickedListener;
    List<HogwartsCharacter> characters;
    Context context;

    public void setOnCharacterClickedListener(OnCharacterClickedListener onCharacterClickedListener) {
        this.onCharacterClickedListener = onCharacterClickedListener;
    }

    public CharacterAdapter(List<HogwartsCharacter> characters, Context context) {
        this.characters = characters;
        this.context = context;
    }

    public void setCharacters(List<HogwartsCharacter> characters) {
        this.characters = characters;
        notifyDataSetChanged();
    }

    public class CharacterViewHolder extends RecyclerView.ViewHolder{

        private TextView characterName;

        public CharacterViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this::characterClicked);   //FALTAVA AIXÒ, cada viewholder o fila té una implementació de la interfície
            characterName = itemView.findViewById(R.id.name);
        }

        private void characterClicked(View view) {
            HogwartsCharacter hogwartsCharacter = characters.get(getAdapterPosition());
            onCharacterClickedListener.onCharacterClicked(hogwartsCharacter);
        }


    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.character_list_row_fragment, parent, false);
        return new CharacterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position) {
        HogwartsCharacter hogwartsCharacter = characters.get(position);
        holder.characterName.setText(hogwartsCharacter.getName());

    }

    @Override
    public int getItemCount() {
        return characters.size();
    }


    public interface OnCharacterClickedListener{
        void onCharacterClicked(HogwartsCharacter hogwartsCharacter);
    }




}
