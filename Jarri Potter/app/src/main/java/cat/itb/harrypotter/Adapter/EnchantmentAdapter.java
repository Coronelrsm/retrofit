package cat.itb.harrypotter.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.harrypotter.R;
import cat.itb.harrypotter.retrofit.HogwartsEnchantment;

public class EnchantmentAdapter extends RecyclerView.Adapter<EnchantmentAdapter.SpellViewHolder> {

    List<HogwartsEnchantment> spells;
    Context context;


    public EnchantmentAdapter(List<HogwartsEnchantment> spells, Context context) {
        this.spells = spells;
        this.context = context;
    }

    public void setSpells(List<HogwartsEnchantment> spells) {
        this.spells = spells;
        notifyDataSetChanged();
    }

    public class SpellViewHolder extends RecyclerView.ViewHolder {

        public TextView spell;

        public SpellViewHolder(@NonNull View itemView) {
            super(itemView);
            spell = itemView.findViewById(R.id.spell);
        }
    }

    @NonNull
    @Override
    public SpellViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enchantment_row_fragment, parent, false);
        return new SpellViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SpellViewHolder holder, int position) {
        HogwartsEnchantment hogwartsEnchantment = spells.get(position);
        holder.spell.setText(hogwartsEnchantment.getSpellName()); //AQUÍ ÉS ON EXCOLLEIXO L'ATRIBUT QUE VULL DEL JSON

    }

    @Override
    public int getItemCount() {
        return spells.size();
    }



}
