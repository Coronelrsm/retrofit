package cat.itb.harrypotter.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HarryPotterService {

    String KEY = "$2a$10$ISAvGBbArpZgLZrCaaMI/esPHPNsgrHmoJe/dwL6aTCSPuAKwNtb6";  //static i final són redundants en interfícies, perquè per defecte ja ho són.
    String GRYFFINDOR = "5a05e2b252f721a3cf2ea33f";
    String RAVENCLAW = "5a05da69d45bd0a11bd5e06f";
    String SLYTHERIN = "5a05dc8cd45bd0a11bd5e071";
    String HUFFLEPUFF = "5a05dc58d45bd0a11bd5e070";




    @GET("sortingHat?key="+KEY)  //no comença amb / ja que la BASE URL acaba amb /
    Call<String> sortingHat(); //NO ÉS UNA LIST!

    @GET("spells?key="+KEY)
    Call<List<HogwartsEnchantment>> getEnchantments();

    @GET("characters?key="+KEY)
    Call<List<HogwartsCharacter>> getAllCharacters();

    @GET("houses/"+GRYFFINDOR+"?key="+KEY)
    Call<List<HogwartsCharacter>> getGryffindorCharacters();

    @GET("houses/"+RAVENCLAW+"?key="+KEY)
    Call<List<HogwartsCharacter>> getRavenCLawCharacters();

    @GET("houses/"+SLYTHERIN+"?key="+KEY)
    Call<List<HogwartsCharacter>> getSlytherinCharacters();

    @GET("houses/"+HUFFLEPUFF+"?key="+KEY)
    Call<List<HogwartsCharacter>> getHufflePuffCharacters();

    @GET("characters/{characterId}"+"?key="+KEY)
    Call<HogwartsCharacter> getCertainCharacter(@Path("characterId") String characterId);

    //GET /characters/{characterId}


    //@GET("users/{username}")
    //Call<User> getUser(@Path("username") String username);



    //houses/{houseId}   els claudators no s'afegeixen
}
