package cat.itb.harrypotter.retrofit;

import com.google.gson.annotations.SerializedName;

public class HogwartsCharacter {

    @SerializedName("_id")
    private String _id;
    @SerializedName("name")
    private String name;
    @SerializedName("role")
    private String role;
    @SerializedName("house")
    private String house;
    @SerializedName("school")
    private String school;
    @SerializedName("ministryOfMagic")
    private boolean ministryOfMagic;
    @SerializedName("orderOfThePhoenix")
    private boolean orderOfThePhoenix;
    @SerializedName("dumbledoresArmy")
    private boolean dumbledoresArmy;
    @SerializedName("deathEater")
    private boolean deathEater;
    @SerializedName("bloodStatus")
    private String bloodStatus;
    @SerializedName("species")
    private String species;

    private String patronus;
    private String alias;
    private String wand;
    private String boggart;
    private String animagus;

    public HogwartsCharacter(String _id, String name, String role, String house, String school, boolean ministryOfMagic, boolean orderOfThePhoenix, boolean dumbledoresArmy, boolean deathEater, String bloodStatus, String species, String patronus, String alias, String wand, String boggart, String animagus) {
        this._id = _id;
        this.name = name;
        this.role = role;
        this.house = house;
        this.school = school;
        this.ministryOfMagic = ministryOfMagic;
        this.orderOfThePhoenix = orderOfThePhoenix;
        this.dumbledoresArmy = dumbledoresArmy;
        this.deathEater = deathEater;
        this.bloodStatus = bloodStatus;
        this.species = species;
        this.patronus = patronus;
        this.alias = alias;
        this.wand = wand;
        this.boggart = boggart;
        this.animagus = animagus;
    }

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public String getHouse() {
        return house;
    }

    public String getSchool() {
        return school;
    }

    public boolean isMinistryOfMagic() {
        return ministryOfMagic;
    }

    public boolean isOrderOfThePhoenix() {
        return orderOfThePhoenix;
    }

    public boolean isDumbledoresArmy() {
        return dumbledoresArmy;
    }

    public boolean isDeathEater() {
        return deathEater;
    }

    public String getBloodStatus() {
        return bloodStatus;
    }

    public String getSpecies() {
        return species;
    }

    public String getPatronus() {
        return patronus;
    }

    public String getAlias() {
        return alias;
    }

    public String getWand() {
        return wand;
    }

    public String getBoggart() {
        return boggart;
    }

    public String getAnimagus() {
        return animagus;
    }
}


