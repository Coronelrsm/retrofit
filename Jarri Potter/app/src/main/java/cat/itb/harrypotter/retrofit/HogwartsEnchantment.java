package cat.itb.harrypotter.retrofit;

import com.google.gson.annotations.SerializedName;

public class HogwartsEnchantment {

    //el     @SerializedName("_id") és el nom que tenen els atributs a la web del Harry Potter

    @SerializedName("_id")
    private String _id;
    @SerializedName("spell")
    private String spellName;
    @SerializedName("type")
    private String type;
    @SerializedName("effect")
    private String effect;

    public HogwartsEnchantment(String _id, String spellName, String type, String effect) {
        this._id = _id;
        this.spellName = spellName;
        this.type = type;
        this.effect = effect;
    }

    public String getSpellName() {
        return spellName;
    }

    public String getType() {
        return type;
    }

    public String getEffect() {
        return effect;
    }
}
