package cat.itb.harrypotter.screens.character;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.harrypotter.Adapter.CharacterAdapter;
import cat.itb.harrypotter.Adapter.EnchantmentAdapter;
import cat.itb.harrypotter.R;
import cat.itb.harrypotter.retrofit.HarryPotterService;
import cat.itb.harrypotter.retrofit.HogwartsCharacter;
import cat.itb.harrypotter.retrofit.HogwartsEnchantment;
import cat.itb.harrypotter.retrofit.RetrofitHarryPotterInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterListFragment extends Fragment {

    @BindView(R.id.character_recycler_view)
    RecyclerView characterRecyclerView;
    private CharacterListViewModel mViewModel;

    private CharacterAdapter characterAdapter;
    private LinearLayoutManager layoutManager;
    HarryPotterService harryPotterService;
    private View globalView;


    private String filter;

    Call<List<HogwartsCharacter>> charactersFromHogwarts;
    private List<HogwartsCharacter> characterList;

    public static CharacterListFragment newInstance() {
        return new CharacterListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.character_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        filter = CharacterListFragmentArgs.fromBundle(getArguments()).getFilter(); //el filtre són les cases o tots els personatges
        //globalView = view;
        harryPotterService = RetrofitHarryPotterInstance.getRetrofit().create(HarryPotterService.class);
        characterList = new ArrayList<>();
        characterAdapter = new CharacterAdapter(characterList, getContext());


    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CharacterListViewModel.class);



        characterRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        characterRecyclerView.setLayoutManager(layoutManager);
        characterRecyclerView.setAdapter(characterAdapter);


        switch (filter) {
            case "all":
                charactersFromHogwarts = harryPotterService.getAllCharacters();
                break;
            case "gryffindor":
                charactersFromHogwarts = harryPotterService.getGryffindorCharacters();
                break;
            case "hufflepuff":
                charactersFromHogwarts = harryPotterService.getHufflePuffCharacters();
                break;
            case "ravenclaw":
                charactersFromHogwarts = harryPotterService.getRavenCLawCharacters();
                break;
            default:
                charactersFromHogwarts = harryPotterService.getSlytherinCharacters();
                break;
        }

        charactersFromHogwarts.enqueue(new Callback<List<HogwartsCharacter>>() {
            @Override
            public void onResponse(Call<List<HogwartsCharacter>> call, Response<List<HogwartsCharacter>> response) {
                characterList = response.body();
                characterAdapter.setCharacters(characterList);

            }

            @Override
            public void onFailure(Call<List<HogwartsCharacter>> call, Throwable t) {

            }
        });


        characterAdapter.setOnCharacterClickedListener(this::watchDetails);


    }

    private void watchDetails (HogwartsCharacter hogwartsCharacter) {
        NavDirections action = CharacterListFragmentDirections.goToCharacterDetailsFragment(hogwartsCharacter.get_id());
        Navigation.findNavController(characterRecyclerView).navigate(action);
        //Navigation.findNavController(globalView).navigate(action);

    }


}
