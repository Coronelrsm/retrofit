package cat.itb.harrypotter.screens.detailsOfACharacter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.harrypotter.R;
import cat.itb.harrypotter.retrofit.HarryPotterService;
import cat.itb.harrypotter.retrofit.HogwartsCharacter;
import cat.itb.harrypotter.retrofit.RetrofitHarryPotterInstance;
import cat.itb.harrypotter.screens.character.CharacterListFragmentArgs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterDetailsFragment extends Fragment {

    private String _id;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.house)
    TextView house;
    @BindView(R.id.patronus)
    TextView patronus;
    @BindView(R.id.species)
    TextView species;
    @BindView(R.id.bloodStatus)
    TextView bloodStatus;
    @BindView(R.id.role)
    TextView role;
    @BindView(R.id.school)
    TextView school;
    @BindView(R.id.deathEater)
    TextView deathEater;
    @BindView(R.id.dumbledoresArmy)
    TextView dumbledoresArmy;
    @BindView(R.id.orderOfThePhoenix)
    TextView orderOfThePhoenix;
    @BindView(R.id.ministryOfMagic)
    TextView ministryOfMagic;
    @BindView(R.id.alias)
    TextView alias;
    @BindView(R.id.wand)
    TextView wand;
    @BindView(R.id.boggart)
    TextView boggart;
    @BindView(R.id.animagus)
    TextView animagus;
    private CharacterDetailsViewModel mViewModel;

    private HarryPotterService harryPotterService;
    Call<HogwartsCharacter> callHogwartsCharacter;
    HogwartsCharacter hogwartsCharacter;


    public static CharacterDetailsFragment newInstance() {
        return new CharacterDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.character_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        _id = CharacterDetailsFragmentArgs.fromBundle(getArguments()).getCharacterIdString();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CharacterDetailsViewModel.class);

        harryPotterService = RetrofitHarryPotterInstance.getRetrofit().create(HarryPotterService.class);
        callHogwartsCharacter = harryPotterService.getCertainCharacter(_id);
        callHogwartsCharacter.enqueue(new Callback<HogwartsCharacter>() {
            @Override
            public void onResponse(Call<HogwartsCharacter> call, Response<HogwartsCharacter> response) {
                hogwartsCharacter = response.body();

                name.setText("Name: " + hogwartsCharacter.getName());
                house.setText("House: " + hogwartsCharacter.getHouse());
                patronus.setText("Patronus: " + hogwartsCharacter.getPatronus());
                species.setText("Species: " + hogwartsCharacter.getSpecies());
                bloodStatus.setText("Blood Status: " + hogwartsCharacter.getBloodStatus());
                role.setText("Role: " + hogwartsCharacter.getRole());
                school.setText("School: " + hogwartsCharacter.getSchool());
                deathEater.setText("Death Eater: " + hogwartsCharacter.isDeathEater());
                dumbledoresArmy.setText("Dumbledore's Army: " + hogwartsCharacter.isDumbledoresArmy());
                orderOfThePhoenix.setText("Order of the Phoenix: " + hogwartsCharacter.isOrderOfThePhoenix());
                ministryOfMagic.setText("Ministry of Magic: " + hogwartsCharacter.isMinistryOfMagic());
                alias.setText("Alias: " + hogwartsCharacter.getAlias());
                wand.setText("Wand: " + hogwartsCharacter.getWand());
                boggart.setText("Boggart: " + hogwartsCharacter.getBoggart());
                animagus.setText("Animagus: " + hogwartsCharacter.getAnimagus());


            }

            @Override
            public void onFailure(Call<HogwartsCharacter> call, Throwable t) {

            }
        });



        /*
        name.setText("Name: " + R.id.name);
        house.setText("House: " + R.id.house);
        patronus.setText("Patronus: " + R.id.patronus);
        species.setText("Species: " + R.id.species);
        bloodStatus.setText("Blood Status: " + R.id.bloodStatus);
        role.setText("Role: " + R.id.role);
        school.setText("School: " + R.id.school);
        deathEater.setText("Death Eater: " + R.id.deathEater);
        dumbledoresArmy.setText("Dumbledore's Army: " + R.id.dumbledoresArmy);
        orderOfThePhoenix.setText("Order of the Phoenix: " + R.id.orderOfThePhoenix);
        ministryOfMagic.setText("Ministry of Magic: " + R.id.ministryOfMagic);
        alias.setText("Alias: " + R.id.alias);
        wand.setText("Wand: " + R.id.wand);
        boggart.setText("Boggart: " + R.id.boggart);
        animagus.setText("Animagus: " + R.id.animagus);
        */



    }

}
