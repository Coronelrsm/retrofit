package cat.itb.harrypotter.screens.enchantment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.harrypotter.Adapter.EnchantmentAdapter;
import cat.itb.harrypotter.R;
import cat.itb.harrypotter.retrofit.HarryPotterService;
import cat.itb.harrypotter.retrofit.HogwartsEnchantment;
import cat.itb.harrypotter.retrofit.RetrofitHarryPotterInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnchantmentFragment extends Fragment {

    @BindView(R.id.enchantment_recycler_view)
    RecyclerView enchantmentRecyclerView;
    private EnchantmentViewModel mViewModel;
    private EnchantmentAdapter enchantmentAdapter;
    private LinearLayoutManager layoutManager;
    HarryPotterService harryPotterService;
    private List<HogwartsEnchantment> spellList;


    public static EnchantmentFragment newInstance() {
        return new EnchantmentFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enchantment_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        harryPotterService = RetrofitHarryPotterInstance.getRetrofit().create(HarryPotterService.class);
        spellList = new ArrayList<>();
        enchantmentAdapter = new EnchantmentAdapter(spellList, getContext());



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(EnchantmentViewModel.class);

        enchantmentRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        enchantmentRecyclerView.setLayoutManager(layoutManager);
        enchantmentRecyclerView.setAdapter(enchantmentAdapter);




        Call<List<HogwartsEnchantment>> enchantmentList = harryPotterService.getEnchantments();
        enchantmentList.enqueue(new Callback<List<HogwartsEnchantment>>() {
            @Override
            public void onResponse(Call<List<HogwartsEnchantment>> call, Response<List<HogwartsEnchantment>> response) {
                spellList = response.body();
                //enchantmentAdapter = new EnchantmentAdapter(spellList, getContext());
                enchantmentAdapter.setSpells(spellList);



            }

            @Override
            public void onFailure(Call<List<HogwartsEnchantment>> call, Throwable t) {

            }
        });


    }

}
