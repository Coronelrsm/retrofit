package cat.itb.harrypotter.screens.enchantment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.harrypotter.R;

public class EnchantmentRowFragment extends Fragment {

    private EnchantmentRowViewModel mViewModel;

    public static EnchantmentRowFragment newInstance() {
        return new EnchantmentRowFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.enchantment_row_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(EnchantmentRowViewModel.class);
        // TODO: Use the ViewModel
    }

}
