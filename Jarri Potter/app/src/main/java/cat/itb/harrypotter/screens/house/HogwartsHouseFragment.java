package cat.itb.harrypotter.screens.house;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.harrypotter.R;

public class HogwartsHouseFragment extends Fragment {

    private HogwartsHouseViewModel mViewModel;

    public static HogwartsHouseFragment newInstance() {
        return new HogwartsHouseFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.hogwarts_house_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(HogwartsHouseViewModel.class);
        // TODO: Use the ViewModel
    }

}
