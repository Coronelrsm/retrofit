package cat.itb.harrypotter.screens.mainScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.harrypotter.R;
import cat.itb.harrypotter.retrofit.HarryPotterService;
import cat.itb.harrypotter.retrofit.RetrofitHarryPotterInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {

    @BindView(R.id.btnSortingHat)
    Button btnSortingHat;
    @BindView(R.id.btnEnchantmentList)
    Button btnEnchantmentList;
    @BindView(R.id.btnShowAllCharacters)
    Button btnShowAllCharacters;
    @BindView(R.id.btnGryffindor)
    Button btnGryffindor;
    @BindView(R.id.btnHufflepuff)
    Button btnHufflepuff;
    @BindView(R.id.btnRavenclaw)
    Button btnRavenclaw;
    @BindView(R.id.btnSlytherin)
    Button btnSlytherin;
    @BindView(R.id.randomHouse)
    TextView randomHouse;
    private MainViewModel mViewModel;
    private String filter;

    View globalView;

    HarryPotterService harryPotterService;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        harryPotterService = RetrofitHarryPotterInstance.getRetrofit().create(HarryPotterService.class);

    }

    @OnClick({R.id.btnSortingHat, R.id.btnEnchantmentList, R.id.btnShowAllCharacters, R.id.btnGryffindor, R.id.btnHufflepuff, R.id.btnRavenclaw, R.id.btnSlytherin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSortingHat:
                Call<String> house = harryPotterService.sortingHat();
                house.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String house = response.body();
                        randomHouse.setText(house);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });

                break;
            case R.id.btnEnchantmentList:
                globalView = view;
                navigateToSpells();
                break;
            case R.id.btnShowAllCharacters:
                globalView = view;
                filter = "all";
                showCharacters();
                break;
            case R.id.btnGryffindor:
                globalView = view;
                filter = "gryffindor";
                showCharacters();
                break;
            case R.id.btnHufflepuff:
                globalView = view;
                filter = "hufflepuff";
                showCharacters();
                break;
            case R.id.btnRavenclaw:
                globalView = view;
                filter = "ravenclaw";
                showCharacters();
                break;
            case R.id.btnSlytherin:
                globalView = view;
                filter = "slytherin";
                showCharacters();
                break;
        }
    }

    private void showCharacters() {
        NavDirections action = MainFragmentDirections.goToCharacterListFragment(filter);
        Navigation.findNavController(globalView).navigate(action);
    }

    private void navigateToSpells() {
        NavDirections action = MainFragmentDirections.goToEnchantmentFragment();
        Navigation.findNavController(globalView).navigate(action);
    }


}
